from django.shortcuts import render
from django.views.generic import TemplateView
# Create your views here.

class IndexView(TemplateView):
    template_name = 'index.pug'

class AboutView(TemplateView):
    template_name = 'about.pug'

class EntryView(TemplateView):
    template_name = 'entry.pug'

class NewsView(TemplateView):
    template_name = 'news.pug'

class newssingleView(TemplateView):
    template_name = 'newsSingle.pug'

class productionView(TemplateView):
    template_name = 'production.pug'

class fcView(TemplateView):
    template_name = 'fc.pug'

class productsView(TemplateView):
    template_name = 'products.pug'

class feedbackView(TemplateView):
    template_name = 'feedback.pug'

class contactsView(TemplateView):
    template_name = 'contacts.pug'