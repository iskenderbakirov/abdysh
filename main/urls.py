from django.urls import path
from main.views import IndexView, AboutView, EntryView, NewsView, newssingleView, productionView, fcView, productsView, feedbackView, contactsView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('about', AboutView.as_view(), name='about'),
    path('entry', EntryView.as_view(), name='entry'),
    path('news', NewsView.as_view(), name='news'),
    path('newssingle', newssingleView.as_view(), name='newssingle'),
    path('production', productionView.as_view(), name='production'),
    path('fc', fcView.as_view(), name='fc'),
    path('products', productsView.as_view(), name='products'),
    path('feedback', feedbackView.as_view(), name='feedback'),
    path('contacts', contactsView.as_view(), name='contacts')
]